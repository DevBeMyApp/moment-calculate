### Calculate in moment.js

Adding and subtracting durations with moment.js

```javascript
 var date = '2016-04-03';
 var format = 'LLLL';
 var result = moment(date).format(format);
```

For more information, please reer to the documentation: http://momentjs.com/docs/#/manipulating/.